\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Introducción}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Robótica móvil}{4}{0}{1}
\beamer@subsectionintoc {1}{2}{SLAM y Visual SLAM}{10}{0}{1}
\beamer@subsectionintoc {1}{3}{Redes Convolucionales}{17}{0}{1}
\beamer@subsectionintoc {1}{4}{Detección de objetos}{19}{0}{1}
\beamer@sectionintoc {2}{Método propuesto}{21}{0}{2}
\beamer@subsectionintoc {2}{1}{Adaptación de la red neuronal}{22}{0}{2}
\beamer@subsectionintoc {2}{2}{Dataset Sintético de entrenamiento}{28}{0}{2}
\beamer@subsectionintoc {2}{3}{Integración del módulo de detección de objetos a S-PTAM}{30}{0}{2}
\beamer@sectionintoc {3}{Evaluación}{34}{0}{3}
\beamer@subsectionintoc {3}{1}{Datasets}{35}{0}{3}
\beamer@subsectionintoc {3}{2}{Resultados}{36}{0}{3}
\beamer@sectionintoc {4}{Conclusiones y trabajo futuro}{42}{0}{4}
